/* START TASK 1: Your code goes here */
const CELLS = [
    [
        document.getElementById('td11'),
        document.getElementById('td12'),
        document.getElementById('td13')
    ], [
        document.getElementById('td21'),
        document.getElementById('td22'),
        document.getElementById('td23')
    ], [
        document.getElementById('td31'),
        document.getElementById('td32'),
        document.getElementById('td33')
    ]
]

CELLS.forEach(arr => {
    arr.forEach(el => {
        el.onclick = () => {
            if(el.style.backgroundColor === '') {
                el.style.backgroundColor = 'yellow'
            } else if (el.style.backgroundColor === 'yellow') {
                el.style.backgroundColor = 'blue'
            } else if (el.style.backgroundColor === 'blue'){
                el.style.backgroundColor = 'green'
            }
        }
    })

    arr[0].onclick = () => {
        if (!arr.find(el => el.style.backgroundColor === 'yellow')) {
            arr.forEach(el => {
                el.style.backgroundColor = 'blue'
            })
        }
    }

    arr.forEach(el => {
        if(el.textContent === 'Special Cell') {
            el.onclick = () => {
                CELLS.forEach(arr => {
                    arr.forEach(el => {
                        el.style.backgroundColor = 'green'
                        document.getElementById('tbl')
                            .style.backgroundColor = 'green'
                    })
                })
            }
        }
    })


})
/* END TASK 1 */

/* START TASK 2: Your code goes here */
const msg = document.getElementById('msg')
const phone = document.getElementById('phone')
const submit = document.getElementById('submit')

const regexNumber = /\+380[0-9]{9}$/

phone.addEventListener('input', () => {
    if (phone.value === '') {
        msg.setStyle({backgroundColor: '#2196f3', border: '#1976d2 solid 3px'})
        msg.textContent = 'Enter your phone number:'
        phone.setStyle({border: 'black solid 3px'})
        submit.disabled = true
    } else if (regexNumber.test(phone.value)) {
        msg.setStyle({backgroundColor: '#4caf50', border: '#2e7d32 solid 3px'})
        msg.textContent = 'You can send'
        phone.setStyle({border: 'black solid 3px'})
        submit.disabled = false
    } else {
        msg.setStyle({backgroundColor: '#f44336', border: '#d32f2f solid 3px'})
        msg.textContent = 'Type number does not follow format +380*********'
        phone.setStyle({border: '#d32f2f solid 3px'})
        submit.disabled = true
    }
})

submit.addEventListener('click', ev => {
    ev.preventDefault()

    msg.setStyle({backgroundColor: '#4caf50', border: '#2e7d32 solid 3px'})
    msg.textContent = 'Data was successfully sent'
    phone.setStyle({border: 'black solid 3px'})
    submit.disabled = false
})

Object.prototype.setStyle = function (style) {
    for (let key in style) {
        if (style.hasOwnProperty(key)) {
            this.style[key] = style[key]
        }
    }
}
/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */
